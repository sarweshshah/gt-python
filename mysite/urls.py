"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

from gotango import views

handler404 = views.my_404_view

urlpatterns = [
    # Admin endpoints
    url(r'^admin/gotango/quote/details/', include('gotango.urls')),
    url(r'^admin/gotango/order/details/', include('gotango.urls')),
    url(r'^admin/', admin.site.urls),

    url(r'^$', views.index),
    url(r'^book/', views.book),
    url(r'^features/', views.features),
    url(r'^terms/', views.terms),
    url(r'^quote/', views.quote),
    url(r'^send_booking/', views.send_booking),
    url(r'^send_inquiry/', views.send_inquiry),
    url(r'^valid_user/', views.valid_user),

]
