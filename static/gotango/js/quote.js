/**
 * Created by sarshah on 24/02/17.
 */
$(document).ready(function () {
    $('select').material_select();

    let $header = $('.collapsible .collapsible-header');
    // Force at least one panel to stay open
    $header.on('click.onepanel', (event) => {
        if ($(event.currentTarget).hasClass('active')) {
            //console.log('Blocking click on active');
            event.preventDefault();
            event.stopPropagation();
        }
        else {
            //if(event.currentTarget.id
            //event.preventDefault();
            //event.stopPropagation();
            if (event.currentTarget.id == "device_info_header") {
                //make all  collapsible haders invalid
                $("#device_info_header").removeData("valid");
                $("#defect_info_header").removeData("valid");
                $("#personal_info_header").removeData("valid");

            }
            else if (event.currentTarget.id == "defect_info_header") {
                //alert($("#device_info_header").data('valid'));
                $("#defect_info_header").removeData("valid");
                $("#personal_info_header").removeData("valid");

                if ($("#device_info_header").data('valid') != 'true') {
                    event.preventDefault();
                    event.stopPropagation();
                }
            }
            else if (event.currentTarget.id == "personal_info_header") {
                $("#personal_info_header").removeData("valid");

                if ($("#defect_info_header").data('valid') != 'true') {
                    event.preventDefault();
                    event.stopPropagation();
                }
            }
        }
    });

    const $defects = $('select[name=defect_dropdown]');

    $("#device_info_submit").find("a").click(function () {
        if ($("#model").val()) {
            $("#device_info_header").data("valid", "true");
            $("#defect_info_header").click();
        }
        else {
            $("#model").addClass("invalid")
                .prop("aria-invalid", "true") //works without this also
                .focus();
        }
    });

    $('#add_new_defect-btn').on('click', function () {
        $('#defect_category').val('Z');
        $('#defect_dropdown').val('Z');
        $('select').material_select();
    });


    $defects.on('change', function () {
        $('#add_new_defect-btn').addClass('scale-in');
        //remove any validation error in display

        $("#defect_error").remove();
        if ($(this).val() != 'Z') {
            var new_defect_name = $(this).children("option:selected").text();
            var new_defect_id = $(this).children("option:selected").attr('value');
            //var new_defect_cost = "Upon inspection";
            var remove_tr_function = "$(this).closest('tr').remove(); ";
            var is_duplicate_defect = false;

            if ($('#selected_defects').length == 0) {
                var selected_defect_empty_table = "<div class='row'>" +
                    "<div class='input-field col s12' style='padding: 10px 10px 0 10px;'>" +
                    "<label for='selected_defects' class='active'>Selected defects (You can select multiple defects)</label>" +
                    "<table id='selected_defects' class='striped responsive-table'>" +
                    "<tbody id='selected_defects_body'></tbody>" +
                    "</table>" +
                    "</div>" +
                    "</div>";

                $('#add_new_defect-btn').closest('.row').after(selected_defect_empty_table);
            }

            //custom attributes are not working use id prefixed with some identifier
            $('#selected_defects').find('td').each(function () {
                if (this.id.match("^tr_defect_")) {
                    var current_id = this.id.replace('tr_defect_', '');

                    //no duplicates
                    if (current_id == new_defect_id) {
                        is_duplicate_defect = true;
                    }
                }
            });

            if (!is_duplicate_defect) {
                //the id of the name td will be <id>
                var new_defect_tr = "<tr>" +
                    "<td class='l8 defect_name' id='tr_defect_" + new_defect_id + "'>" + new_defect_name +
                    // "</td><td class='l3 defect_price'>" + new_defect_cost +
                    "</td><td class='l1 right defect_actions'><i " +
                    "onclick=" + remove_tr_function +
                    "class='mdi mdi-delete' " +
                    "style ='color: grey'></i>" +
                    "</td>" +
                    " </tr>";

                $('#selected_defects_body').append(new_defect_tr).fadeIn();
            }
        }
    });


    //For getting CSRF token
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    $("#submit").click(function (e) {
        e.preventDefault();

        const csrftoken = getCookie('csrftoken');
        let selected_defect_ids = [];

        $('#selected_defects').find('td').each(function () {
            if (this.id.match("^tr_defect_")) {
                var current_id = this.id.replace('tr_defect_', '');
                selected_defect_ids.push(current_id);
            }
        });

        if ($("#phone_number").val() && !$("#phone_number").hasClass("invalid")
            && $("#email").val() && !$("#email").hasClass("invalid")) {

            $("#personal_info_header").data("valid", "true");
            $.ajax({
                url: window.location.href, // the endpoint,commonly same url
                type: "POST", // http method
                url: '/send_inquiry/',
                data: {
                    csrfmiddlewaretoken: csrftoken,
                    name: "Quote Form",
                    form: $("#quote_form").serialize(),
                    defect_ids: JSON.stringify(selected_defect_ids)
                }, // data sent with the post request

                // handle a successful response
                success: function (json) {
                    console.log(json); // another sanity check
                    //On success show the data posted to server as a message
                    //alert('Order with id ' + json['id'] + ' has been created');
                    //$('#results').html("<p>Order with id " + json['id'] + " has been created</p>");
                    $("#modal_success_message").html("<p>Quote with id " + json["id"] + " has been created</p>");
                    $("#success_modal").modal({dismissible: false}).modal("open");

                },

                // handle a non-successful response
                error: function (xhr, errmsg, err) {
                    console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
                    $("#modal_error_message").html("<p>Oops something is not right. Please contact our support team.</p>");
                    $("#error_modal").modal({dismissible: false}).modal("open");
                }
            });
        }
        else {

            if (!$("#email").val() || $("#email").hasClass("invalid")) {

                $("#email").removeClass("valid").addClass("invalid");
            }

            if (!$("#phone_number").val() || $("#phone_number").hasClass("invalid")) {

                $("#phone_number").removeClass("valid").addClass("invalid");
            }

            //override phone number focus if both phone and email are invalid

            if ($("#email").hasClass("invalid")) {
                $("#email").focus();
            } else if ($("#phone_number").hasClass("invalid")) {
                $("#phone_number").focus();
            }
            else {
                //shouldn't happen
            }
        }


    });

});