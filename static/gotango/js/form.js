/**
 * Created by sarshah on 24/02/17.
 */
$(document).ready(function () {
    $('select').material_select();


    let $header = $('.collapsible .collapsible-header');
    let $defects = $('select[name=defect_dropdown]');

    // Force at least one panel to stay open
    $header.on('click.onepanel', (event) => {
        if ($(event.currentTarget).hasClass('active')) {
            event.preventDefault();
            event.stopPropagation();
        }
        else {
            //if(event.currentTarget.id
            //event.preventDefault();
            //event.stopPropagation();
            if (event.currentTarget.id == "device_info_header") {
                //make all  collapsible haders invalid
                $("#device_info_header").removeData("valid");
                $("#defect_info_header").removeData("valid");
                $("#personal_info_header").removeData("valid");
                $("#pickup_info_header").removeData("valid");
            }
            else if (event.currentTarget.id == "defect_info_header") {
                //alert($("#device_info_header").data('valid'));
                $("#defect_info_header").removeData("valid");
                $("#personal_info_header").removeData("valid");
                $("#pickup_info_header").removeData("valid");
                if ($("#device_info_header").data('valid') != 'true') {
                    event.preventDefault();
                    event.stopPropagation();
                }
            }
            else if (event.currentTarget.id == "personal_info_header") {
                $("#personal_info_header").removeData("valid");
                $("#pickup_info_header").removeData("valid");
                if ($("#defect_info_header").data('valid') != 'true') {
                    event.preventDefault();
                    event.stopPropagation();
                }
            }
            else if (event.currentTarget.id == "pickup_info_header") {
                $("#pickup_info_header").removeData("valid");
                if ($("#personal_info_header").data('valid') != 'true') {
                    event.preventDefault();
                    event.stopPropagation();
                }
            }

        }
    });

    $("#device_info_submit").find("a").click(function () {
        if ($("#model").val()) {
            $("#device_info_header").data("valid", "true");
            $("#defect_info_header").click();
        }
        else {
            $("#model").addClass("invalid")
                .prop("aria-invalid", "true") //works without this also
                .focus();
        }
    });

    $("#personal_info_submit").find("a").click(function () {

        if ($("#phone_number").val() && !$("#phone_number").hasClass("invalid")
            && $("#email").val() && !$("#email").hasClass("invalid")) {

            $("#personal_info_header").data("valid", "true");
            $("#pickup_info_header").click();
        }
        else {
            //IMP : do not put alerts as jquery is not able to set invalid class 
            //IMP .focus messed up error of email field , so add focus only in the end
            if (!$("#email").val() || $("#email").hasClass("invalid")) {
                $("#email").removeClass("valid").addClass("invalid");
            }

            if (!$("#phone_number").val() || $("#phone_number").hasClass("invalid")) {
                $("#phone_number").removeClass("valid").addClass("invalid");
            }

            //override phone number focus if both phone and email are invalid
            if ($("#email").hasClass("invalid")) {
                $("#email").focus();
            } else if ($("#phone_number").hasClass("invalid")) {
                $("#phone_number").focus();
            }
        }
    });

    $('#add_new_defect-btn').on('click', function () {
        $('#defect_category').val('Z');
        $('#defect_dropdown').val('Z');
        $('select').material_select();
    });

    $defects.on('change', function () {
        $('#add_new_defect-btn').addClass('scale-in');

        //remove any validation error in display
        $("#defect_error").remove();

        if ($(this).val() != 'Z') {
            let new_defect_name = $(this).children("option:selected").text();
            let new_defect_id = $(this).children("option:selected").attr('value');
            let remove_tr_function = "$(this).closest('tr').remove(); ";
            let is_duplicate_defect = false;

            if ($('#selected_defects').length == 0) {
                var selected_defect_empty_table = "<div class='row'>" +
                    "<div class='input-field col s12' style='padding: 10px 10px 0 10px;'>" +
                    "<label for='selected_defects' class='active'>Selected defects (You can select multiple defects)</label>" +
                    "<table id='selected_defects' class='striped responsive-table'>" +
                    "<tbody id='selected_defects_body'></tbody>" +
                    "</table>" +
                    "</div>" +
                    "</div>";

                $('#add_new_defect-btn').closest('.row').after(selected_defect_empty_table);
            }

            //custom attributes are not working use id prefixed with some identifier
            $('#selected_defects').find('td').each(function () {
                if (this.id.match("^tr_defect_")) {
                    const current_id = this.id.replace('tr_defect_', '');

                    //no duplicates
                    if (current_id == new_defect_id) {
                        is_duplicate_defect = true;
                    }
                }
            });

            if (!is_duplicate_defect) {
                //the id of the name td will be <id>
                let new_defect_tr = "<tr>" +
                    "<td class='l8 defect_name' id='tr_defect_" + new_defect_id + "'>" + new_defect_name +
                    // "</td><td class='l3 defect_price'>" + new_defect_cost +
                    "</td><td class='l1 right defect_actions'><i " +
                    "onclick=" + remove_tr_function +
                    "class='mdi mdi-delete' " +
                    "style ='color: grey'></i>" +
                    "</td></tr>";

                $('#selected_defects_body').append(new_defect_tr).fadeIn();
            }
        }
    });

    // Date picker options
    $('.datepicker').pickadate({
        min: new Date(), // Restricting user to select pickup dates before today
        dateFormat: 'yyyy-mm-dd',
        onSet: function (arg) {
            if ('select' in arg) { //prevent closing on selecting month/year
                this.close();
            }
        }
    });

    //For getting CSRF token
    function getCookie(name) {
        let cookieValue = null;
        if (document.cookie && document.cookie != '') {
            const cookies = document.cookie.split(';');
            for (let i = 0; i < cookies.length; i++) {
                const cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    $("#submit").click(function (e) {
        e.preventDefault();

        let csrftoken = getCookie('csrftoken');
        let selected_defect_ids = [];

        $('#selected_defects').find('td').each(function () {
            if (this.id.match("^tr_defect_")) {
                const current_id = this.id.replace('tr_defect_', '');
                selected_defect_ids.push(current_id);
            }
        });

        if ($("#address1").val() && !$("#address1").hasClass("invalid")
            && $("#area").val() && !$("#area").hasClass("invalid")) {

            $("#pickup_info_header").data("valid", "true");

            $.ajax({
                url: window.location.href, // the endpoint,commonly same url
                type: "POST", // http method
                url: '/send_booking/',
                data: {
                    csrfmiddlewaretoken: csrftoken,
                    name: "Booking Form",
                    form: $("#booking_form").serialize(),
                    defect_ids: JSON.stringify(selected_defect_ids)
                }, // data sent with the post request

                // handle a successful response
                success: function (json) {
                    //On success show the data posted to server as a message
                    $("#modal_success_message").html("<p>Order with id " + json["id"] + " has been created</p>");
                    $("#success_modal").modal({dismissible: false}).modal("open");

                },

                // handle a non-successful response
                error: function () {
                    $("#modal_error_message").html("<p>Oops something is not right. Please contact our support team.</p>");
                    $("#error_modal").modal({dismissible: false}).modal("open");
                }
            });
        }
        else {

            if (!$("#address1").val() || $("#address1").hasClass("invalid")) {
                $("#address1").removeClass("valid").addClass("invalid");
            }

            if (!$("#area").val() || $("#area").hasClass("invalid")) {
                $("#area").removeClass("valid").addClass("invalid");
            }

            if ($("#address1").hasClass("invalid")) {
                $("#address1").focus();
            } else if ($("#area").hasClass("invalid")) {
                $("#area").focus();
            }
        }
    });

});