from __future__ import unicode_literals

from django.apps import AppConfig


class GotangoConfig(AppConfig):
    name = 'gotango'

    def ready(self):
    	import signals
