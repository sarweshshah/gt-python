# helper functions
import datetime
import logging

from constants import *
from models import *


# DoesNotExist exception for get
def save_device(model, color):
    # errorlogger = logging.getLogger('django.request')
    debuglogger = logging.getLogger('django.debug')
    print 'Before Device Saved ', ' model is ', 'xxx' + model + 'xxx'
    try:
        assert model
    except AssertionError:
        debuglogger.debug('Device not added')
    # errorlogger.error('Device not added')
    assert model.strip()
    try:
        device = Device.objects.get(model_number=model)
    except Device.DoesNotExist:
        device = Device.objects.get(model_number=constants.OTHER_MOBILE_MODEL)
    device_identity = DeviceIdentity(device_id=device, color=color)
    device_identity.save()
    print 'Device Saved ', device_identity
    return device_identity


def validate_user(email, phone):
    assert email and email.strip()
    assert phone and phone.strip()

    is_existing_user, is_new_user, email_mismatch, phone_mismatch = False, False, False, False
    user = None
    print 'email', email, 'phone', phone
    try:
        user = User.objects.get(registered_phone_number=phone)
        if user.primary_email == email:
            is_existing_user = True
        else:
            email_mismatch = True
    except User.DoesNotExist:
        try:
            user = User.objects.get(primary_email=email)
            phone_mismatch = True
        except User.DoesNotExist:
            is_new_user = True

    print 'is_existing_user', is_existing_user, 'is_new_user', is_new_user, 'email_mismatch', email_mismatch, 'phone_mismatch', phone_mismatch
    if user:
        print 'user', user
    return is_existing_user, is_new_user, email_mismatch, phone_mismatch, user


def save_user(name, email, phone):
    user_contact = None
    user = None
    is_existing_user, is_new_user, email_mismatch, phone_mismatch, user = validate_user(email, phone)
    if is_existing_user:
        pass
    elif is_new_user:
        user = User(name=name, primary_email=email, registered_phone_number=phone,
                    registration_date=datetime.date.today())
        user.save()
    elif email_mismatch:
        user_contact = UserContact(user_id=user, contact_type=CONTACT_TYPE[0][0], contact_value=email)
        user_contact.save()
    elif phone_mismatch:
        user_contact = UserContact(user_id=user, contact_type=CONTACT_TYPE[1][0], contact_value=phone)
        user_contact.save()

    return is_existing_user, is_new_user, email_mismatch, phone_mismatch, user, user_contact


def save_address(user, address_line1, address_line2, landmark, area, address_type):
    assert user
    assert address_line1 and address_line1.strip()
    assert area

    user_address = UserAddress(user_id=user, first_line=address_line1, second_line=address_line2, landmark=landmark,
                               area=area, address_type=address_type)
    user_address.save()

    return user_address


def save_order_defects(order, defects):
    assert order
    assert defects and len(defects) > 0

    order_defects = []

    for defect in defects:
        order_defect = OrderDefect(order_id=order, defect_id=defect)

        order_defect.save()
        order_defects.append(order_defect)

    return order_defects


def save_quote_defects(quote, defects):
    assert quote
    assert defects and len(defects) > 0

    quote_defects = []

    for defect in defects:
        quote_defect = QuoteDefect(quote_id=quote, defect_id=defect)

        quote_defect.save()
        quote_defects.append(quote_defect)

    return quote_defects


def save_order(user, device_identity, pickup_date, user_address, defect_description, service_center_type, in_warranty,
               standby, phone_number):
    assert user
    assert user_address

    order = Order(user_id=user, create_time=datetime.datetime.now(), device_identity_id=device_identity,
                  pickup_date=pickup_date, pickup_address_id=user_address, defect_description=defect_description,
                  service_center_type=service_center_type, in_warranty=in_warranty, standby=standby,
                  phone_number=phone_number)
    order.save()

    return order


def save_quote(user, model, query, phone_number, city):
    assert user
    assert model
    assert model.strip()

    try:
        device = Device.objects.get(model_number=model)
    except Device.DoesNotExist:
        device = Device.objects.get(model_number=constants.OTHER_MOBILE_MODEL)
    quote = Quote(user_id=user, create_time=datetime.datetime.now(), device_id=device,
                  query=query,
                  phone_number=phone_number, city=city)
    quote.save()

    return quote


def get_defects(defect_ids):
    assert defect_ids and len(defect_ids) > 0

    return Defect.objects.filter(pk__in=defect_ids)
