insert into gotango_defectcategory (id, defect_category) values

(1, 'Touch and Display'),
(2, 'Charging'),
(3, 'Software'), 
(4, 'Network'),
(5, 'Sound'), 
(6, 'Camera'), 
(7, 'Water damage'),
(8, 'Frame Housing and Buttons'),
(9, 'Sensor'),
(10, 'Other');