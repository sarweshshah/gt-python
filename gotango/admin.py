from django.contrib import admin
from django.core.urlresolvers import reverse
from gotango.common.util import encryptor

from .models import Quote, Order, Device, DeviceIdentity, Brand, Gadget, Defect, User, UserAddress, Partner, \
    PartnerSupports, PartnerContact, UserContact, StandbyDevice, Incentive, Employee, EmployeeContact, \
    OrderAppliedIncentive, OrderDefect, Repair, DevicePart, RepairPartsMap, PartnerParts, OrderJobSheet, \
    OrderJobSheetDeviceHistory, QuoteDefect

#admin.site.index_template = "gotango/admin/admin_index.html"

admin.site.register(Device)
admin.site.register(DeviceIdentity)
admin.site.register(Brand)
admin.site.register(Gadget)
admin.site.register(Defect)
admin.site.register(User)
admin.site.register(UserAddress)
admin.site.register(PartnerSupports)
admin.site.register(PartnerContact)
admin.site.register(Partner)
admin.site.register(UserContact)
admin.site.register(StandbyDevice)
admin.site.register(Incentive)
admin.site.register(Employee)
admin.site.register(EmployeeContact)
admin.site.register(OrderDefect)
admin.site.register(OrderAppliedIncentive)
admin.site.register(OrderJobSheet)
admin.site.register(OrderJobSheetDeviceHistory)
admin.site.register(RepairPartsMap)
admin.site.register(DevicePart)
admin.site.register(PartnerParts)
admin.site.register(QuoteDefect)

class OrderAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'encrypted_id','user_id', 'device_identity_id', 'create_time', 'details'
    )
    readonly_fields = ['encrypted_id']
    search_fields = ['id', 'user_id__name', 'user_id__primary_email', 'user_id__registered_phone_number', 'encrypted_id']

    def details(self, obj):
        return '<a href="%s" class="btn btn-default">details</a>' % reverse('orderDetails', args=[obj.pk])

    # return '<button name="button" value="OK" type="button">details</button>'
    details.allow_tags = True
    details.short_description = 'Details'

    #behaviour should be to match the text in the search box on any of the search fields defined
    #if the results are empty and the text is a valid id, then proceed to decrypt the id and filter on it
    #python2 super expects classname and self 
    def get_search_results(self, request, queryset, search_term):
        queryset, use_distinct = super(OrderAdmin, self).get_search_results(request, queryset, search_term)

        if len(queryset):
            #do nothing
            pass
        else:
            try:
                queryset = self.model.objects.filter(id=encryptor.decrypt(search_term))
            except TypeError as e:
                #when text is non base32 
                print 'Unable to decrypt id'
            else:
                pass
            finally:
                pass
        
        print queryset, use_distinct 
        return queryset, use_distinct


admin.site.register(Order, OrderAdmin)


class RepairAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'device_id', 'defect_id', 'estimated_cost'
    )
    search_fields = ['device_id__model_number', ]


admin.site.register(Repair, RepairAdmin)


class QuoteAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'encrypted_id', 'device_id', 'user_id', 'user_id_registered_phone_number', 'create_time', 'city', 'is_addressed',
        'details'
    )
    search_fields = ['device_id__model_number', 'encrypted_id', 'user_id__name', 'user_id__primary_email',
                     'user_id__registered_phone_number']

    def user_id_registered_phone_number(self, obj):
        return obj.user_id.registered_phone_number

    user_id_registered_phone_number.allow_tags = True
    user_id_registered_phone_number.short_description = 'Phone Number'

    def details(self, obj):
        return '<a href="%s" class="btn btn-default">details</a>' % reverse('quoteDetails', args=[obj.pk])

    details.allow_tags = True
    details.short_description = 'Details'


admin.site.register(Quote, QuoteAdmin)
