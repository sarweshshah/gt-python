AREAS_IN_BANGALORE = \
((1,'Bellandur'),
(2,'Whitefield'),
(3,'Domlur'),
(4,'Btm Layout'),
(5,'Kundanahalli'),
(6,'Jayanagar'),
(7,'Mahadevapura'),
(8,'HAL'),
(9,'Munnekolala'),
(10,'Indiranagar'),
(11,'Hsr Layout'),
(12,'Nagwara'),
(13,'Thubarahalli'),
(14,'Marathahalli'),
(15,'Hebbal'),
(16,'Varthur'),
(17,'Sarjapur Main road'),
(18,'Koramangala'),
(19,'Kadubeesanahalli'),
(20,'Ulsoor'),
(21,'Kadugodi'),
(22,'Old Madras Road'),
(23,'Krpuram'),
(24,'Benniganahalli'),
(25,'Ramamurthy Nagar'),
(26,'Doddanekundi'),
(27,'Bommanahalli'),
(28,'Kaikondrahalli'),
(29,'Kasavanahalli'),
(30,'Yemalur'),
(31,'CV Raman Nagar'),
(32,'Vimanapura'),
(33,'Kudlu Gate'),
(34,'Hopefarm'),
(35,'MG Road'),
(36,'Kadugudi'),
(37,'Kammanahalli'),
(38,'Chinnapanahalli'),
(39,'Brookefield'),
(40,'JP Nagar'),
(41,'Electronic City'),
(42,'Haralur Road'),
(43,'Kaggadasapura'),
(44,'Hoodi'),
(45,'JC Road'),
(46,'Mahadevpura'),
(47,'Frazer Town'),
(48,'Shanthinagar'),
(49,'Malleshpalya'),
(50,'Bannerghatta Road'),
(51,'Doddabanaswadi'),
(52,'HRBR Layout'),
(53,'Old Airport Road'),
(54,'Channasandra'),
(55,'BEL Road'),
(56,'Ejipura'),
(57,'Murugeshpalya'),
(58,'Silk Board'),
(59,'Jeevan Bheema Nagar'),
(60,'Kasturi Nagar'),
(61,'Lingarajpuram'),
(62,'Cunningham Road'),
(63,'ITPL'),
(64,'Dooravani Nagar'),
(65,'Basavanagar'),
(66,'Doddakanahalli'),
(67,'Panathur main road'),
(68,'Ramagondannahalli'),
(69,'Hosur Road'),
(70,'Jakkur'),
(71,'Hennur'),
(72,'Udaya Nagar'),
(73,'Residency Road'),
(74,'Teacher\'s Colony'),
(75,'Seegehalli'),
(76,'B Narayanapura'),
(77,'EPIP Zone'),
(78,'Dairy Circle'),
(79,'Banaswadi'),
(80,'Tin Factory'),
(81,'Brigade Road'),
(82,'Tavarekere'),
(83,'Cubbon Road'),
(84,'AECS Layout'),
(85,'Basavanagara'),
(86,'Battarahalli'),
(87,'BEML Layout'),
(88,'Bhoganhalli'),
(89,'Doddenakundi'),
(90,'Garudachar Palya'),
(91,'GM Palya'),
(92,'Green Glen Layout'),
(93,'Hoysala Nagar'),
(94,'Kariyammana Agrahara'),
(95,'Kodihalli'),
(96,'New Tippasandra'),
(97,'Karthik Nagar'),
(98,'Vibhutipura'),
(99,'Vignana Nagar'),
(100, 'Other'))

OTHER_AREA = 'Other'