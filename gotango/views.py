import json

from dateutil import parser
from django.contrib.admin.views.decorators import staff_member_required
from django.core import serializers
from django.http import HttpResponse, JsonResponse, QueryDict
from django.shortcuts import get_object_or_404, render
from django.shortcuts import (
    render_to_response
)
from django.template import RequestContext

from areas import *
from functions import *


# Create your views here.



def index(request):
    logger = logging.getLogger('django')
    logger.info('HOMEPAGE')
    return render(request, 'index.html')


def features(request):
    logger = logging.getLogger('django')
    logger.info('Features')
    return render(request, 'features/index.html')


def terms(request):
    logger = logging.getLogger('django')
    logger.info('Terms')
    return render(request, 'terms/index.html')


def tomorrow():
    return datetime.date.today() + datetime.timedelta(days=1)


def send_inquiry(request):
    logger = logging.getLogger('django')

    print 'defects body ', request.POST.get('selected_defects_body')
    logger.info('Inquiry SEND')

    print 'before the post check'
    if request.method == 'POST':
        print 'inside the post check'
        # https://impythonist.wordpress.com/2015/06/16/django-with-ajax-a-modern-client-server-communication-practise/
        # POST goes here . is_ajax is must to capture ajax requests. Beginner's pit.
        if request.is_ajax():
            print 'name of form ', request.POST.get('name')
            print 'name of form ', request.POST.get('model')

            form_data = QueryDict(request.POST['form'])

            # use json loads to convert string to list
            defect_ids = json.loads(request.POST.get('defect_ids'))

            model = form_data.get('model')
            query = form_data.get('query')

            name = form_data.get('customer_name')
            email = form_data.get('email')
            phone_number = form_data.get('phone_number')

            city = form_data.get('city')
            if city:
                print 'city is ', city
            else:
                city = constants.OTHER_CITY
                print 'city defaulted to ', city

            print 'model is ', model

            print 'query is', query

            is_existing_user, is_new_user, email_mismatch, phone_mismatch, user, user_contact = save_user(name, email,
                                                                                                          phone_number)
            logger.info(user_contact)

            print 'is_existing_user', is_existing_user, 'is_new_user', is_new_user, 'email_mismatch', email_mismatch, 'phone_mismatch', phone_mismatch
            print 'user', user


            quote = save_quote(user, model, query, phone_number, city)

            print 'quote id', quote.encrypted_id

            defects = get_defects(defect_ids)

            logger.info(defects)

            quote_defects = save_quote_defects(quote, defects)

            logger.info(quote_defects)
            data = {"success": True, "id": quote.encrypted_id}
            # Returning same data back to browser.It is not possible with Normal submit
            return JsonResponse(data)



def book(request):
    device_dict = {}
    area_dict = {}
    logger = logging.getLogger('django')
    logger.info('BOOKING')
    gadget_list = Gadget.objects.all()
    defect_list = Defect.objects.all()
    defect_category_list = DefectCategory.objects.all()
    device_list = Device.objects.all()
    areas_in_bangalore = areas.AREAS_IN_BANGALORE
    for device in device_list:
        device_dict[device.model_number] = None
    for area in areas_in_bangalore:
        area_dict[area[1]] = None
    return render(request, 'order/form.html',
                  {'gadget_list': gadget_list, 'defect_list': defect_list, 'device_list': json.dumps(device_dict),
                   'service_center_types': constants.SERVICE_CENTER_TYPE,
                   'areas': json.dumps(area_dict), 'defect_category_list': defect_category_list})


def quote(request):
    device_dict = {}
    logger = logging.getLogger('django')
    logger.info('BOOKING')
    gadget_list = Gadget.objects.all()
    defect_list = Defect.objects.all()
    defect_category_list = DefectCategory.objects.all()
    device_list = Device.objects.all()
    for device in device_list:
        device_dict[device.model_number] = None
    return render(request, 'quote/quote.html',
                  {'gadget_list': gadget_list, 'defect_list': defect_list, 'device_list': json.dumps(device_dict),
                   'service_center_types': constants.SERVICE_CENTER_TYPE,
                   'cities': constants.CITY, 'defect_category_list': defect_category_list})


def valid_user(request):
    print 'inside validate user'
    if request.method == 'POST':
        if request.is_ajax():
            print 'inside validate user'
            form_data = QueryDict(request.POST['form'])

            name = form_data.get('first_name')
            email = form_data.get('email')
            phone_number = form_data.get('phone_number')

            is_existing_user, is_new_user, email_mismatch, phone_mismatch, user = validate_user(email, phone_number)

            if email_mismatch:
                custom_error_msg = "This email is registered with another number"
            elif phone_mismatch:
                custom_error_msg = "This phone is registered with another email"
            else:
                custom_error_msg = ""
            response = {"custom_error_msg": custom_error_msg, "is_existing_user": is_existing_user,
                        "is_new_user": is_new_user}
            # Returning same data back to browser.It is not possible with Normal submit
            return JsonResponse(response)


def send_booking(request):
    logger = logging.getLogger('django')
    errorlogger = logging.getLogger('django.request')

    area_dict = dict((y, x) for x, y in AREAS_IN_BANGALORE)
    # print 'area dictionary', area_dict

    print 'defects body ', request.POST.get('selected_defects_body')
    logger.info('BOOKING SEND')

    print 'before the post check'
    if request.method == 'POST':
        print 'inside the post check'
        # https://impythonist.wordpress.com/2015/06/16/django-with-ajax-a-modern-client-server-communication-practise/
        # POST goes here . is_ajax is must to capture ajax requests. Beginner's pit.
        if request.is_ajax():
            print 'name of form ', request.POST.get('name')
            print 'name of form ', request.POST.get('model')

            form_data = QueryDict(request.POST['form'])

            # use json loads to convert string to list
            defect_ids = json.loads(request.POST.get('defect_ids'))

            model = form_data.get('model')
            color = form_data.get('color')
            defect_description = form_data.get('defect_description')
            service_center_type = form_data.get('service_center')
            warranty = form_data.get('is_under_warranty') == 'on'

            name = form_data.get('customer_name')
            email = form_data.get('email')
            phone_number = form_data.get('phone_number')

            address_line1 = form_data.get('address1')
            address_line2 = form_data.get('address2')
            landmark = form_data.get('landmark')
            area = form_data.get('area')
            if form_data.get('address_type') == 'on':
                address_type = constants.OFFICE
            else:
                address_type = constants.HOME

            pickup_date = form_data.get('pickup_date')
            print 'pickup date is ', pickup_date
            if pickup_date:
                formatted_pickup_date = parser.parse(pickup_date).date()
            else:
                formatted_pickup_date = tomorrow()

            standby = form_data.get('standby') == "on"

            print 'model is ', model, 'color is ', color

            device_identity = save_device(model, color)

            print 'defect_description is', defect_description, 'service_center_type is', service_center_type, 'warranty is', warranty

            is_existing_user, is_new_user, email_mismatch, phone_mismatch, user, user_contact = save_user(name, email,
                                                                                                          phone_number)
            logger.info(user_contact)

            print 'is_existing_user', is_existing_user, 'is_new_user', is_new_user, 'email_mismatch', email_mismatch, 'phone_mismatch', phone_mismatch
            print 'user', user

            print 'area is ', area
            try:
                area_key = area_dict[area]
            except KeyError:
                area_key = area_dict[areas.OTHER_AREA]
            user_address = save_address(user, address_line1, address_line2, landmark, area_key, address_type)
            print 'user address', user_address

            order = save_order(user, device_identity, formatted_pickup_date, user_address, defect_description,
                               service_center_type, warranty, standby, phone_number)

            print 'order id', order.encrypted_id

            defects = get_defects(defect_ids)

            logger.info(defects)

            order_defects = save_order_defects(order, defects)

            logger.info(order_defects)
            data = {"success": True, "id": order.encrypted_id}
            # Returning same data back to browser.It is not possible with Normal submit
            return JsonResponse(data)




def my_404_view(request):
    response = render_to_response(
        'errors/404.html',
        context=RequestContext(request)
    )
    response.status_code = 404

    return response


########################################
# admin views
@staff_member_required
def simpleOrderDetails(request, order_id):
    return HttpResponse("You're looking at order %s." % order_id)


@staff_member_required
def orderDetails(request, order_id):
    order = get_object_or_404(Order, pk=order_id)
    order_details = order
    user = order.user_id
    device_identity = order.device_identity_id

    # logger.info(user.registered_phone_number)
    order_details.id = order.id
    order_details.encrypted_id = order.encrypted_id
    order_details.user_id = user
    order_details.user_phone = user.registered_phone_number
    order_details.time_created = order.create_time
    order_details.defect_desc = order.defect_description
    order_details.user_name = user.name
    order_details.user_email = user.primary_email
    order_details.device = device_identity.device_id
    order_details.color = device_identity.color
    order_details.service_center_type = order.service_center_type
    order_details.warranty = order.in_warranty
    order_details.pickup_date = order.pickup_date
    order_details.pickup_address_first_line = order.pickup_address_id.first_line
    order_details.pickup_address_second_line = order.pickup_address_id.second_line
    order_details.pickup_address_landmark = order.pickup_address_id.landmark
    order_details.pickup_address_area = order.pickup_address_id.area
    order_details.standby = order.standby
    order_details.actual_cost = order.actual_cost
    order_details.status = order.status
    order_details.parent_order_id = order.parent_order_id
    order_details.admin_comments = order.admin_comments
    order_details.phone_number = order.phone_number
    # TODO take data from order job sheet and job device history tables

    incentivelist = list(OrderAppliedIncentive.objects.filter(order_id=order.id))
    usercontactlist = list(UserContact.objects.filter(user_id=user))

    order_details.incentivelist = incentivelist
    order_details.usercontactlist = usercontactlist
    defectlist = list(OrderDefect.objects.filter(order_id=order.id))
    order_details.defectlist = defectlist
    return render(request, 'gotango/order_details.html', {

        'order_details': order_details,
    })


@staff_member_required
def quoteDetails(request, quote_id):
    quote = get_object_or_404(Quote, pk=quote_id)
    quote_details = quote
    user = quote.user_id

    # logger.info(user.registered_phone_number)
    quote_details.id = quote.id
    quote_details.encrypted_id = quote.encrypted_id
    quote_details.user_id = quote.user_id
    quote_details.user_phone = user.registered_phone_number
    quote_details.time_created = quote.create_time
    quote_details.user_name = user.name
    quote_details.user_email = user.primary_email
    quote_details.device = quote.device_id
    quote_details.query = quote.query
    quote_details.city = quote.city
    quote_details.admin_comments = quote.admin_comments
    quote_details.is_addressed = quote.is_addressed
    quote_details.phone_number = quote.phone_number
    usercontactlist = list(UserContact.objects.filter(user_id=quote.user_id))
    quote_details.usercontactlist = usercontactlist
    defectlist = list(QuoteDefect.objects.filter(quote_id=quote.id))
    quote_details.defectlist = defectlist

    return render(request, 'gotango/quote_details.html', {
        'quote_details': quote_details,
    })
