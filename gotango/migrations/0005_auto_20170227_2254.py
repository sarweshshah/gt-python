# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-02-27 17:24
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gotango', '0004_quote_phone_number'),
    ]

    operations = [
        migrations.AlterField(
            model_name='quote',
            name='city',
            field=models.IntegerField(blank=True, choices=[(0, b'Bangalore'), (1, b'Mumbai'), (2, b'Delhi'), (3, b'Pune'), (4, b'Kolkata'), (5, b'Chennai'), (6, b'Hyderabad'), (7, b'Other')], null=True),
        ),
    ]
