# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-02-26 06:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gotango', '0002_auto_20170219_0029'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='phone_number',
            field=models.CharField(default='None', max_length=20),
        ),
        migrations.AlterField(
            model_name='order',
            name='service_center_type',
            field=models.CharField(choices=[(b'G', b'GoTango workshop'), (b'A', b'Authorised service center')], max_length=1),
        ),
        migrations.AlterField(
            model_name='partner',
            name='area',
            field=models.IntegerField(choices=[(1, b'Bellandur'), (2, b'Whitefield'), (3, b'Domlur'), (4, b'Btm Layout'), (5, b'Kundanahalli'), (6, b'Jayanagar'), (7, b'Mahadevapura'), (8, b'HAL'), (9, b'Munnekolala'), (10, b'Indiranagar'), (11, b'Hsr Layout'), (12, b'Nagwara'), (13, b'Thubarahalli'), (14, b'Marathahalli'), (15, b'Hebbal'), (16, b'Varthur'), (17, b'Sarjapur Main road'), (18, b'Koramangala'), (19, b'Kadubeesanahalli'), (20, b'Ulsoor'), (21, b'Kadugodi'), (22, b'Old Madras Road'), (23, b'Krpuram'), (24, b'Benniganahalli'), (25, b'Ramamurthy Nagar'), (26, b'Doddanekundi'), (27, b'Bommanahalli'), (28, b'Kaikondrahalli'), (29, b'Kasavanahalli'), (30, b'Yemalur'), (31, b'CV Raman Nagar'), (32, b'Vimanapura'), (33, b'Kudlu Gate'), (34, b'Hopefarm'), (35, b'MG Road'), (36, b'Kadugudi'), (37, b'Kammanahalli'), (38, b'Chinnapanahalli'), (39, b'Brookefield'), (40, b'JP Nagar'), (41, b'Electronic City'), (42, b'Haralur Road'), (43, b'Kaggadasapura'), (44, b'Hoodi'), (45, b'JC Road'), (46, b'Mahadevpura'), (47, b'Frazer Town'), (48, b'Shanthinagar'), (49, b'Malleshpalya'), (50, b'Bannerghatta Road'), (51, b'Doddabanaswadi'), (52, b'HRBR Layout'), (53, b'Old Airport Road'), (54, b'Channasandra'), (55, b'BEL Road'), (56, b'Ejipura'), (57, b'Murugeshpalya'), (58, b'Silk Board'), (59, b'Jeevan Bheema Nagar'), (60, b'Kasturi Nagar'), (61, b'Lingarajpuram'), (62, b'Cunningham Road'), (63, b'ITPL'), (64, b'Dooravani Nagar'), (65, b'Basavanagar'), (66, b'Doddakanahalli'), (67, b'Panathur main road'), (68, b'Ramagondannahalli'), (69, b'Hosur Road'), (70, b'Jakkur'), (71, b'Hennur'), (72, b'Udaya Nagar'), (73, b'Residency Road'), (74, b"Teacher's Colony"), (75, b'Seegehalli'), (76, b'B Narayanapura'), (77, b'EPIP Zone'), (78, b'Dairy Circle'), (79, b'Banaswadi'), (80, b'Tin Factory'), (81, b'Brigade Road'), (82, b'Tavarekere'), (83, b'Cubbon Road'), (84, b'AECS Layout'), (85, b'Basavanagara'), (86, b'Battarahalli'), (87, b'BEML Layout'), (88, b'Bhoganhalli'), (89, b'Doddenakundi'), (90, b'Garudachar Palya'), (91, b'GM Palya'), (92, b'Green Glen Layout'), (93, b'Hoysala Nagar'), (94, b'Kariyammana Agrahara'), (95, b'Kodihalli'), (96, b'New Tippasandra'), (97, b'Karthik Nagar'), (98, b'Vibhutipura'), (99, b'Vignana Nagar')]),
        ),
        migrations.AlterField(
            model_name='useraddress',
            name='area',
            field=models.IntegerField(choices=[(1, b'Bellandur'), (2, b'Whitefield'), (3, b'Domlur'), (4, b'Btm Layout'), (5, b'Kundanahalli'), (6, b'Jayanagar'), (7, b'Mahadevapura'), (8, b'HAL'), (9, b'Munnekolala'), (10, b'Indiranagar'), (11, b'Hsr Layout'), (12, b'Nagwara'), (13, b'Thubarahalli'), (14, b'Marathahalli'), (15, b'Hebbal'), (16, b'Varthur'), (17, b'Sarjapur Main road'), (18, b'Koramangala'), (19, b'Kadubeesanahalli'), (20, b'Ulsoor'), (21, b'Kadugodi'), (22, b'Old Madras Road'), (23, b'Krpuram'), (24, b'Benniganahalli'), (25, b'Ramamurthy Nagar'), (26, b'Doddanekundi'), (27, b'Bommanahalli'), (28, b'Kaikondrahalli'), (29, b'Kasavanahalli'), (30, b'Yemalur'), (31, b'CV Raman Nagar'), (32, b'Vimanapura'), (33, b'Kudlu Gate'), (34, b'Hopefarm'), (35, b'MG Road'), (36, b'Kadugudi'), (37, b'Kammanahalli'), (38, b'Chinnapanahalli'), (39, b'Brookefield'), (40, b'JP Nagar'), (41, b'Electronic City'), (42, b'Haralur Road'), (43, b'Kaggadasapura'), (44, b'Hoodi'), (45, b'JC Road'), (46, b'Mahadevpura'), (47, b'Frazer Town'), (48, b'Shanthinagar'), (49, b'Malleshpalya'), (50, b'Bannerghatta Road'), (51, b'Doddabanaswadi'), (52, b'HRBR Layout'), (53, b'Old Airport Road'), (54, b'Channasandra'), (55, b'BEL Road'), (56, b'Ejipura'), (57, b'Murugeshpalya'), (58, b'Silk Board'), (59, b'Jeevan Bheema Nagar'), (60, b'Kasturi Nagar'), (61, b'Lingarajpuram'), (62, b'Cunningham Road'), (63, b'ITPL'), (64, b'Dooravani Nagar'), (65, b'Basavanagar'), (66, b'Doddakanahalli'), (67, b'Panathur main road'), (68, b'Ramagondannahalli'), (69, b'Hosur Road'), (70, b'Jakkur'), (71, b'Hennur'), (72, b'Udaya Nagar'), (73, b'Residency Road'), (74, b"Teacher's Colony"), (75, b'Seegehalli'), (76, b'B Narayanapura'), (77, b'EPIP Zone'), (78, b'Dairy Circle'), (79, b'Banaswadi'), (80, b'Tin Factory'), (81, b'Brigade Road'), (82, b'Tavarekere'), (83, b'Cubbon Road'), (84, b'AECS Layout'), (85, b'Basavanagara'), (86, b'Battarahalli'), (87, b'BEML Layout'), (88, b'Bhoganhalli'), (89, b'Doddenakundi'), (90, b'Garudachar Palya'), (91, b'GM Palya'), (92, b'Green Glen Layout'), (93, b'Hoysala Nagar'), (94, b'Kariyammana Agrahara'), (95, b'Kodihalli'), (96, b'New Tippasandra'), (97, b'Karthik Nagar'), (98, b'Vibhutipura'), (99, b'Vignana Nagar')]),
        ),
    ]
