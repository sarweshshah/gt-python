from __future__ import unicode_literals

from django.db import models

import constants, areas


class User(models.Model):
    name = models.CharField(max_length=50, blank=False)
    primary_email = models.EmailField(unique=True, blank=False)
    registered_phone_number = models.CharField(max_length=20, unique=True, blank=False)
    registration_date = models.DateField('date user registered')

    def __str__(self):
        return self.name


class UserAddress(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    first_line = models.CharField(max_length=100)
    second_line = models.CharField(max_length=100, blank=True, null=True)
    landmark = models.CharField(max_length=50, blank=True, null=True)
    area = models.IntegerField(choices=areas.AREAS_IN_BANGALORE)
    city = models.CharField(max_length=25, default=constants.BANGALORE)
    state = models.CharField(max_length=25, default=constants.KARNATAKA)
    zip = models.IntegerField(blank=True, null=True)
    address_type = models.CharField(max_length=1, choices=constants.ADDRESS_TYPE, blank=True, null=True)

    def __str__(self):
        return self.user_id.name


class Gadget(models.Model):
    name = models.CharField(max_length=15, unique=True)

    def __str__(self):
        return self.name


class Brand(models.Model):
    gadget_id = models.ForeignKey(Gadget, on_delete=models.CASCADE)
    name = models.CharField(max_length=15)

    def __str__(self):
        return self.gadget_id.name + " " + self.name


class Device(models.Model):
    brand_id = models.ForeignKey(Brand, on_delete=models.CASCADE)
    model_number = models.CharField(max_length=40, unique=True)

    def __str__(self):
        return self.model_number


class DeviceIdentity(models.Model):
    device_id = models.ForeignKey(Device, on_delete=models.CASCADE)
    color = models.CharField(max_length=15, blank=True, null=True)
    imei = models.CharField(max_length=17, blank=True, null=True)

    def __str__(self):
        return self.device_id.model_number

class DefectCategory(models.Model):
    defect_category = models.CharField(max_length=30)

    def __str__(self):
        return self.defect_category


class Defect(models.Model):
    defect_name = models.CharField(max_length=50)
    defect_category_id = models.ForeignKey(DefectCategory, on_delete=models.CASCADE)
    defect_info = models.CharField(max_length=250, blank=True, null=True)

    def __str__(self):
        return self.defect_name


class Incentive(models.Model):
    incentive_id = models.CharField(max_length=25, primary_key=True)
    incentive_type = models.IntegerField(choices=constants.INCENTIVE_TYPE)
    incentive_amount = models.IntegerField()
    validity_start_time = models.DateTimeField()
    validity_end_time = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.incentive_id


class Employee(models.Model):
    role = models.IntegerField(choices=constants.EMPLOYEE_ROLE)
    name = models.CharField(max_length=50)
    employment_status = models.IntegerField(choices=constants.EMPLOYMENT_STATUS)
    primary_email = models.EmailField(unique=True)
    registered_phone_number = models.CharField(max_length=20, unique=True)
    joining_date = models.DateTimeField()
    termination_date = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.name


class EmployeeContact(models.Model):
    employee_id = models.ForeignKey(Employee, on_delete=models.CASCADE)
    contact_type = models.CharField(max_length=1, choices=constants.CONTACT_TYPE)
    contact_value = models.CharField(max_length=50)

    def __str__(self):
        return Employee.objects.get(id=self.employee_id.id).name


class UserContact(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    contact_type = models.CharField(max_length=1, choices=constants.CONTACT_TYPE)
    contact_value = models.CharField(max_length=50)

    def __str__(self):
        return str(self.user_id.id) + " " + str(self.contact_type)


class Partner(models.Model):
    category = models.CharField(max_length=1, choices=constants.PARTNER_CATEGORY)
    rating = models.IntegerField(null=True, blank=True)
    name = models.CharField(max_length=50)
    poc = models.CharField(max_length=50)
    area = models.IntegerField(choices=areas.AREAS_IN_BANGALORE)

    def __str__(self):
        return self.name


class PartnerSupports(models.Model):
    partner_id = models.ForeignKey(Partner, on_delete=models.CASCADE)
    brand_id = models.ForeignKey(Brand, on_delete=models.CASCADE)
    warranty = models.BooleanField()
    rating = models.IntegerField(null=True, blank=True)


class PartnerContact(models.Model):
    partner_id = models.ForeignKey(Partner, on_delete=models.CASCADE)
    contact_type = models.CharField(max_length=1, choices=constants.CONTACT_TYPE)
    contact_value = models.CharField(max_length=50)

    def __str__(self):
        return self.partner_id.name + " " + self.contact_type


class Order(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    device_identity_id = models.ForeignKey(DeviceIdentity, on_delete=models.CASCADE)
    create_time = models.DateTimeField()
    service_center_type = models.CharField(max_length=1, choices=constants.SERVICE_CENTER_TYPE)
    in_warranty = models.BooleanField()
    pickup_date = models.DateField()
    pickup_address_id = models.ForeignKey(UserAddress, on_delete=models.CASCADE, related_name='pickup_address')
    delivery_date = models.DateField(null=True, blank=True)
    delivery_address_id = models.ForeignKey(UserAddress, on_delete=models.CASCADE, null=True, blank=True,
                                            related_name='delivery_address')
    standby = models.BooleanField(default=False)
    expected_cost = models.IntegerField(blank=True, null=True)
    actual_cost = models.IntegerField(blank=True, null=True)
    status = models.CharField(max_length=15, default="Placed")
    defect_description = models.CharField(max_length=200, blank=True, null=True)
    channel = models.CharField(max_length=2, choices=constants.CHANNEL_NAMES, blank=True, null=True)
    parent_order_id = models.IntegerField(blank=True, null=True)
    admin_comments = models.CharField(max_length=200, null=True, blank=True)
    encrypted_id = models.CharField(max_length=20, unique=True, null=True, blank=True)
    phone_number = models.CharField(max_length=20, default="None")
    def __str__(self):
        return str(self.id)


class StandbyDevice(models.Model):
    device_identity_id = models.ForeignKey(DeviceIdentity, on_delete=models.CASCADE)
    device_type = models.IntegerField(choices=constants.STANDBY_DEVICE_TYPE, default=constants.SMARTPHONE)
    assigned_order_id = models.ForeignKey(Order, on_delete=models.CASCADE, blank=True, null=True)
    status = models.IntegerField(choices=constants.STANDBY_DEVICE_STATUS)
    issue_date = models.DateTimeField(blank=True, null=True)
    expected_release_date = models.DateTimeField(blank=True, null=True)
    release_date = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.device_identity_id.device_id.model_number


class OrderAppliedIncentive(models.Model):
    order_id = models.ForeignKey(Order, on_delete=models.CASCADE)
    incentive_id = models.ForeignKey(Incentive, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.order_id) + "::" + str(self.incentive_id)


class OrderDefect(models.Model):
    order_id = models.ForeignKey(Order, on_delete=models.CASCADE)
    defect_id = models.ForeignKey(Defect, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.order_id.id) + " " + self.defect_id.defect_name


class Quote(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    device_id = models.ForeignKey(Device, on_delete=models.CASCADE, null=True, blank=True)
    city = models.IntegerField(choices=constants.CITY, null=True, blank=True)
    create_time = models.DateTimeField()
    query = models.CharField(max_length=200, null=True, blank=True)
    admin_comments = models.CharField(max_length=200, null=True, blank=True)
    is_addressed = models.BooleanField(default=False)
    channel = models.CharField(max_length=2, choices=constants.CHANNEL_NAMES, null=True, blank=True)
    encrypted_id = models.CharField(max_length=20, unique=True, null=True, blank=True)
    phone_number = models.CharField(max_length=20, default="None")

    def __str__(self):
        return str(self.id) + "::" + self.encrypted_id


class QuoteDefect(models.Model):
    quote_id = models.ForeignKey(Quote, on_delete=models.CASCADE)
    defect_id = models.ForeignKey(Defect, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.quote_id.id) + " " + self.defect_id.defect_name


class Repair(models.Model):
    defect_id = models.ForeignKey(Defect, on_delete=models.CASCADE)
    device_id = models.ForeignKey(Device, on_delete=models.CASCADE)
    repair_type = models.IntegerField(choices=constants.REPAIR_TYPE)
    estimated_cost = models.IntegerField(null=True, blank=True)
    warranty_in_days = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.device_id.model_number + self.defect_id.defect_name


class DevicePart(models.Model):
    name = models.CharField(max_length=30)
    color = models.CharField(max_length=15, blank=True, null=True)
    quality = models.CharField(max_length=1, choices=constants.DEVICE_PART_QUALITY, blank=True, null=True)

    def __str__(self):
        return self.name


class RepairPartsMap(models.Model):
    repair_id = models.ForeignKey(Repair, on_delete=models.CASCADE)
    part_id = models.ForeignKey(DevicePart, on_delete=models.CASCADE)


class PartnerParts(models.Model):
    partner_id = models.ForeignKey(Partner, on_delete=models.CASCADE)
    part_id = models.ForeignKey(DevicePart, on_delete=models.CASCADE)
    cost = models.IntegerField()
    warranty_in_days = models.IntegerField(blank=True, null=True)
    availability_in_days = models.IntegerField(blank=True, null=True)
    rating = models.IntegerField(blank=True, null=True)
    device_id = models.ForeignKey(Device, on_delete=models.CASCADE)


class OrderJobSheet(models.Model):
    order_id = models.ForeignKey(Order, on_delete=models.CASCADE)
    defect_id = models.ForeignKey(Defect, on_delete=models.CASCADE)
    repair_id = models.ForeignKey(Repair, on_delete=models.CASCADE, null=True, blank=True)
    cost = models.IntegerField(blank=True, null=True)
    partner_id = models.ForeignKey(Partner, on_delete=models.CASCADE)
    status = models.CharField(max_length=1, choices=constants.ORDER_JOB_SHEET_STATUS)
    outcome = models.CharField(max_length=1, choices=constants.ORDER_JOB_SHEET_OUTCOME, null=True, blank=True)
    time_created = models.DateTimeField()
    time_updated = models.DateTimeField()
    external_job_sheet_id = models.CharField(max_length=30, blank=True, null=True)

    def __str__(self):
        return str(self.order_id.id) + self.repair_id.device_id.model_number + self.repair_id.defect_id.defect_type


class OrderJobSheetDeviceHistory(models.Model):
    order_id = models.ForeignKey(Order, on_delete=models.CASCADE)
    order_job_sheet_id = models.ForeignKey(OrderJobSheet, on_delete=models.CASCADE, blank=True, null=True)
    stage = models.CharField(max_length=1, choices=constants.ORDER_JOB_SHEET_DEVICE_HISTORY_STAGE)
    attribute_key = models.IntegerField(choices=constants.ORDER_JOB_SHEET_DEVICE_HISTORY_STAGE_ATTRIBUTE_KEY)
    attribute_value = models.CharField(max_length=100)

    def __str__(self):
        return str(self.order_id.id) + self.stage
