from django.conf.urls import url

from . import views

urlpatterns = [
    # ex: /order/5/details/
    url(r'^order/(?P<order_id>[0-9]+)/$', views.orderDetails, name='orderDetails'),
    # ex: /quote/5/details/
    url(r'^quote/(?P<quote_id>[0-9]+)/$', views.quoteDetails, name='quoteDetails'),
]
