from django.db.models.signals import post_save
from django.dispatch import receiver

from gotango.common.util import encryptor

from models import *

@receiver(post_save, sender=Order)
def update_encrypted_order_id(sender, instance, **kwargs):
	if instance.encrypted_id is None or len(instance.encrypted_id) == 0:
		instance.encrypted_id = str('O-' + encryptor.encrypt(str(instance.id)))
		#print 'XXXXXXX  order with this id saved', encryptor.encrypt(str(instance.id))
		instance.save()
	else:
		pass


@receiver(post_save, sender=Quote)
def update_encrypted_quote_id(sender, instance, **kwargs):
	if instance.encrypted_id is None or len(instance.encrypted_id) == 0:
		instance.encrypted_id = str('Q-' + encryptor.encrypt(str(instance.id)))
		instance.save()
	else:
		pass