#AES is a preferred encryption over DES , however DES is used as the block size of DES is smaller and will result in smaller size encrypted text
#AES encrypts data in block of size 16 whereas DES in 8
#Both algos are implemented here
#source : http://www.laurentluce.com/posts/python-and-cryptography-with-pycrypto/

from Crypto.Cipher import AES, DES
import base64
import os

from gotango.common import constants

def encrypt(text):
	
	pad = lambda s: s + (constants.DES_BLOCK_SIZE - len(s) % constants.DES_BLOCK_SIZE) * constants.DES_PADDING

	encryptDES = lambda c, s: base64.b32encode(c.encrypt(pad(s)))

	cipher = DES.new(constants.DES_SECRET_KEY, DES.MODE_CFB, constants.DES_INITIALIZATION_VECTOR)

	encryptedText = encryptDES(cipher, text).rstrip(constants.BASE32_PADDING)

	print 'Encrypted Text:', encryptedText

	return encryptedText


def decrypt(encryptedText):

	pad = lambda s: s + (constants.BASE32_BLOCK_SIZE - len(s) % constants.BASE32_BLOCK_SIZE) * constants.BASE32_PADDING

	decryptDES = lambda c, s: c.decrypt(base64.b32decode(pad(s)))

	cipher = DES.new(constants.DES_SECRET_KEY, DES.MODE_CFB, constants.DES_INITIALIZATION_VECTOR)

	decryptedText = decryptDES(cipher, encryptedText).rstrip(constants.DES_PADDING)

	print 'Dexrypted Text:', decryptedText

	return decryptedText


###############
###AES implementation
# def encryption(privateInfo):
# 	BLOCK_SIZE = 16
# 	PADDING = '{'
	
# 	pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * PADDING

# 	print 'padded: ', pad(privateInfo)
	
# 	EncodeAES = lambda c, s: base64.b64encode(c.encrypt(pad(s)))

# 	#secret = os.urandom(BLOCK_SIZE)
# 	#print base64.b64encode(secret).decode('utf-8')
# 	#secret = '0+Zmr8HUyquFFW9IsqptEQ=='
# 	secret = '0+GoT@ng0quFFW9IsqptEQ=='

# 	print 'encryption key:', secret

# 	cipher = AES.new(secret)

# 	encoded = EncodeAES(cipher, privateInfo)

# 	print 'Encrypted string:', encoded


# def decryption(privateInfo):
# 	PADDING = '{'
# 	DecodeAES = lambda c, e: c.decrypt(base64.b64decode(e)).rstrip(PADDING)
# 	secret = '0+GoT@ng0quFFW9IsqptEQ=='
# 	cipher = AES.new(secret)
# 	decoded = DecodeAES(cipher, privateInfo)
# 	print 'Decrypted string:', decoded





