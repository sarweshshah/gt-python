
#DES encryption algo uses 8 byte iv and key
#Ideally this should be encrypted text and not stored in code
DES_INITIALIZATION_VECTOR = 'GoT@ng0!'
DES_SECRET_KEY = 'cEZTM8qj'
DES_BLOCK_SIZE = 8

#use padding in case input text size is not a multiple of block size
DES_PADDING = '#'

#base 32 encoding uses = for padding
BASE32_PADDING = '='
BASE32_BLOCK_SIZE = 16