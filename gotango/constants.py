SMARTPHONE = 0
BASIC_PHONE = 1
BANGALORE = 'Bangalore'
KARNATAKA = 'Karnataka'
OTHER = 0
OTHER_MOBILE_MODEL = 'Other Mobile Model'
TOUCHANDDISPLAY = 1
CHARGING = 2
HOME = 'H'
OFFICE = 'O'

STANDBY_DEVICE_TYPE = ((SMARTPHONE, 'Smartphone'), (BASIC_PHONE, 'Basic Phone'))
STANDBY_DEVICE_STATUS = ((0, 'Available'), (1, 'Unavailable'))
DEFECT_CATEGORY = (
    (OTHER, 'Other'), (TOUCHANDDISPLAY, 'Touch and Display'), (CHARGING, 'Charging'), (3, 'Software'), (4, 'Network'),
    (5, 'Sound'), (6, 'Camera'), (7, 'Water damage'), (8, 'Frame Housing and Buttons'), (9, 'Sensor'))
INCENTIVE_TYPE = ((0, 'Coupon'), (1, 'Referral'))
EMPLOYEE_ROLE = ((0, 'Technician'), (1, 'Tango'))
EMPLOYMENT_STATUS = ((0, 'Active'), (1, 'Inactive'))
CONTACT_TYPE = (('E', 'Email'), ('P', 'Mobile number'), ('L', 'Landline'))
PARTNER_CATEGORY = (('S', 'GoTango certified service center'), ('D', 'Distributor'), ('A', 'Authorised service center'),
                    ('G', 'GoTango workshop'))
CHANNEL_NAMES = (('Po', 'Poster'), ('Go', 'Google'), ('Jd', 'Justdial'))
SERVICE_CENTER_TYPE = (('G', 'GoTango workshop'), ('A', 'Authorised service center'))
REPAIR_TYPE = ((0, 'Touch glass replacement'), (1, 'Touch and display replacement'), (2, 'Display replacement'),
               (3, 'Battery replacement'), (4, 'Charging pin replacement'), (5, 'Diagnostic'), (6, 'Software flash'),
               (7, 'Mic replacement'), (8, 'Ear spaeaker replacement'), (9, 'Loudspeaker replacement'),
               (10, 'Sim slot replacement'), (11, 'Headphone jack replacement'), (12, 'Primary camera replacement'),
               (13, 'Front camera replacement'), (14, 'Primary camera cleaning'), (15, 'Front camera cleaning'),
               (16, 'Backpanel replacement'), (17, 'Power button replacement'), (18, 'Volume button replacement'),
               (19, 'Chemical cleaning'))
DEVICE_PART_QUALITY = ((0, 'Original'), (1, 'Compatible'))
ORDER_JOB_SHEET_STATUS = (('R', 'In Repair'), ('C', 'Completed'))
ORDER_JOB_SHEET_OUTCOME = (('R', 'Repaired'), ('N', 'Not Repaired'))
ORDER_JOB_SHEET_DEVICE_HISTORY_STAGE = (('P', 'Pickup'), ('R', 'In repair'))
ORDER_JOB_SHEET_DEVICE_HISTORY_STAGE_ATTRIBUTE_KEY = ((0, 'Pickup Tango'), (1, 'Pickup Time'))
CITY = ((0, 'Bangalore'), (1, 'Mumbai'), (2, 'Delhi'), (3, 'Pune'), (4, 'Kolkata'), (5, 'Chennai'), (6, 'Hyderabad'),
        (7, 'Other'))
OTHER_CITY = 7
ADDRESS_TYPE = ((HOME, 'Home'), (OFFICE, 'Office'))
